<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\myService;

class GenusController extends Controller
{
    /**
     * @Route("/genus", methods="GET",name="index")
     */
    public function indexAction(Request $request)
    {
        $product = new Product();
        $product->setName('thisProduct');
        $product->setPrice('5.7');
        $product->setDescription('dsfiaos<br>bbbbb\nccccc');
        
        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($product);

        $em->flush();
        return new Response('yoyo');
    }

    /**
     * @Route("/genus/{pid}", name="genus_pid", requirements={"pid": "\d+"})
     */
    public function showAction($pid)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product = $repo->find($pid);
        $product2 = $repo->getFirst();
        dump($product);
        dump($product2);
        
        if(!$product){
            throw $this->createNotFoundException('Cant find this pid:'.$pid);
        }
        
        $res = new JsonResponse();
        $res->setData($product);
                
        
        return $res;
    }

    /**
     * @Route("/genus/add")
     */
    public function addAction()
    {
        $cate = new Category();
        $cate->setName('goods');
        
        $product = new Product();
        $product->setName('haveCate');
        $product->setDescription('vvvv');
        $product->setPrice('6.6');
        
        $product->setCategory($cate);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($cate);
        $em->persist($product);
        $em->flush();
        
        return new Response('category_id='.$cate->getId().', product_id='.$product->getId());
        
        //        $this->getDoctrine()->getRepository('App')-
    }

    /**
     * @Route("/genus/sel")
     */
    public function selAction()
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findProductWithCate(2);
        dump($product);
        
        dump($product->getCategory());
        return new Response('');
    }

    /**
     * @Route("/genus/serv")
     */
    public function servAction()
    {
        $serv = $this->get('app.serv');
        dump($serv->showParam1());
        
        return new Response('');
    }

    /**
     * @Route("genus/servmanager")
     */
    public function servManagerAction()
    {
        $main = $this->get('app.serv_manager');
        return new Response($main->dumpAll());
    }

    /**
     * @Route("genus/event")
     */
    public function eventAction()
    {
        $logger = $this->get('logger');
        $logger->warning('inController');
        
        return new Response('<html><head></head><body></body></html>');
    }
}
