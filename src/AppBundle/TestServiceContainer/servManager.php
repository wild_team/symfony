<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2017/2/18
 * Time: 下午2:45
 */

namespace AppBundle\TestServiceContainer;
use AppBundle\TestServiceContainer\myServiceInterface;
use Psr\Log\LoggerInterface;

class servManager
{
    
    private $servs;
    private $logger;
    
    function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function addServ(myServiceInterface $serv)
    {
        $this->logger->warning('hellllo');
        
        if( ! $serv instanceof myServiceInterface)
        {
            $this->logger->warning('wrong serv interface');
        }
        $this->servs[] = $serv;
    }
    
    public function dumpAll()
    {
        $res = '';
        foreach ($this->servs as $serv)
        {
            $res .= $serv-showParam1().'<br>';
        }
        
        return $res;
    }
}