<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2017/2/18
 * Time: 上午11:10
 */

namespace AppBundle\TestServiceContainer;

class myService implements myServiceInterface
{
    private $param1;
    function __construct($param1)
    {
        $this->param1 = $param1;
//        log('this is param1='.$param1);
    }

    public function showParam1()
    {
        return 'service1 message:'.$this->param1;
    }
}