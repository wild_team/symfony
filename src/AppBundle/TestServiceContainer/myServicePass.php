<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2017/2/18
 * Time: 上午11:10
 */

namespace AppBundle\TestServiceContainer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class myServicePass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        
        $servs = $container->findTaggedServiceIds('serv.type');

        $definition = $container->getDefinition('app.serv_manager');

        foreach ($servs as $id => $tags) {
            $definition->addMethodCall(
                'addServ',
                array(new Reference($id))
            );
        }
//        $container->getDefinition('app.servManager')->addMethodCall('addServ', $servs);
    }
}