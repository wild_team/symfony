<?php
/**
 * Created by PhpStorm.
 * User: wild
 * Date: 2017/2/18
 * Time: 上午11:10
 */

namespace AppBundle\TestServiceContainer;


class myServiceMain
{
    protected $serv;
    function __construct()
    {
//        log('this is param1='.$param1);
    }
    
    public function getServ(myService $serv)
    {
        if($serv instanceof myService){
            $this->serv = $serv;
        }
    }
    
    public function showParam()
    {
        return $this->serv->showParam1();
    }
}